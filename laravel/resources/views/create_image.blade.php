@extends('layouts.app')

@section('title', 'Crear imagen')

@section('content')

 
               
        <div class="row">
            <div class="col-md-7 offset-md-2">    
                @if($errors->has('fail'))
                    <div class='col-md-12 form_alert'>
                        <div>
                            <p class='alert alert-danger' role='alert' style='font-size: 20px;text-align: center'><b>{{$errors->first('fail')}}</b></p>
                        </div>
                    </div>    
                @elseif($errors->has('successfull'))
                    <div class='col-md-12 form_alert'>
                        <div>
                            <p class='alert alert-success' role='alert' style='font-size: 20px;text-align: center'><b>{{$errors->first('successfull')}}</b></p>
                        </div>
                    </div>    
                @endif
            </div>   
        </div>   
    
    <div class=row>
        <div class="col-md-7 offset-md-2">
        <form id="form_create" method="POST" action="{{route('image.store')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="title">Título de la imagen:</label>
                <input id="form_create_image_title" type="text" class="form-control" name="title" value="" required="required"/>
            </div>
            @if($errors->has('title'))
            <div class="row">
                <div class="col-md-7 offset-md-2">
                    <div class='col-md-12 form_alert'>
                        <div>
                            <p class='alert alert-danger' role='alert' style='font-size: 15px;text-align: center'><b>{{$errors->first('title')}}</b></p>
                        </div>
                    </div>
                </div>
            </div>        
            @endif        
            <div class="form-group row">
                <div class="col-md-7">
                    <label for="category">Categoría de la imagen:</label>
                    <select id="form_create_image_category" class="form-control" name="category">
                        @foreach($categories as $category)
                            @if($loop->index == 0)
                                <option selected="selected" value="{{$category->name}}">{{$category->name}}</option>
                            @else
                                <option value="{{$category->name}}">{{$category->name}}</option>
                            @endif       
                        @endforeach
                    </select>
                </div> 
                <div class="col-md-3 offset-2">
                    <button id="btn-modal-category" type="button" class="btn btn-primary" data-toggle="modal" data-target="#categoryModal">
                        Crear categoría
                    </button>
                </div>       
            </div>
            @if($errors->has('category'))
                <div class="row">
                    <div class="col-md-7 offset-md-2">
                        <div class='col-md-12 form_alert'>
                            <div>
                                <p class='alert alert-danger' role='alert' style='font-size: 15px;text-align: center'><b>{{$errors->first('category')}}</b></p>
                            </div>
                        </div>
                    </div>
                </div> 
            @endif    
                               
            <div class="form-group">
                <label for="description">Descripción de la imagen:</label>
                <textarea id="form_create_image_description" class="form-control" name="description" rows="3" required="required"></textarea>
            </div>
            <div class="form-group">
                <label for="url">Imagen:</label>
                <input id="form_create_image_file" type="file" class="form-control-file load_image_file" name="url" required="required">

            </div>
            <div class="form-group row">
                <div class="col-md-5">
                    <img src="" alt="" class="img-thumbnail form_image_preview"> 
                </div>    
            </div>
            @if($errors->has('incorrect_format'))
                <div class="row">
                    <div class="col-md-7 offset-md-2">
                        <div class='col-md-12 form_alert'>
                        <div>
                            <p class='alert alert-danger' role='alert' style='font-size: 15px;text-align: center'><b>{{$errors->first('incorrect_format')}}</b></p>
                        </div>
                        </div>
                    </div>
                </div>  
            @endif
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12 offset-md-4">
                    <a href="{{route('image.index')}}" class="form-control btn btn-outline-dark btn-group-actions">Listado de imágenes</a> 
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <input type="submit" id="create_image" value="Crear" class="form-control btn btn-primary btn-group-actions"/> 
                </div>
            </div>

            
                       
        </form>
        </div>
    </div>    

    @include('modal')

@endsection