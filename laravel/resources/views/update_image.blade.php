@extends('layouts.app')

@section('title', 'Modificar imagen')

@section('content')

    <div class="row">
        <div class="col-md-7 offset-md-2">
            <div id="div_form_update_image">
                @if($errors->has('image_category'))
                    <div class='col-md-12 form_alert'>
                        <div>
                            <p class='alert alert-danger' role='alert' style='font-size: 20px;text-align: center'><b>{{$errors->first('image_category')}}</b></p>
                        </div>
                    </div>
                @elseif($errors->has('image_title'))
                    <div class='col-md-12 form_alert'>
                        <div>
                            <p class='alert alert-danger' role='alert' style='font-size: 20px;text-align: center'><b>{{$errors->first('image_title')}}</b></p>
                        </div>
                    </div>
                @elseif($errors->has('incorrect_format'))
                    <div class='col-md-12 form_alert'>
                        <div>
                            <p class='alert alert-danger' role='alert' style='font-size: 20px;text-align: center'><b>{{$errors->first('incorrect_format')}}</b></p>
                        </div>
                    </div>
                @elseif($errors->has('fail'))
                    <div class='col-md-12 form_alert'>
                        <div>
                            <p class='alert alert-danger' role='alert' style='font-size: 20px;text-align: center'><b>{{$errors->first('fail')}}</b></p>
                        </div>
                    </div>    
                @elseif($errors->has('successfull'))
                    <div class='col-md-12 form_alert'>
                        <div>
                            <p class='alert alert-success' role='alert' style='font-size: 20px;text-align: center'><b>{{$errors->first('successfull')}}</b></p>
                        </div>
                    </div>    
                @endif
            </div>   
        </div>   
    </div>
    <div class=row>
        <div class="col-md-7 offset-md-2">
        <form id="form_update" method="POST" action="{{route('imageUpdate', $image->id)}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="image_title">Título de la imagen:</label>
                <input id="form_update_image_title" type="text" class="form-control" name="image_title" value="{{$image->title}}"/>
            </div>
            <div class="form-group">
                <label for="image_category">Categoría de la imagen:</label>
                <select id="form_update_image_category" class="form-control" name="image_category">
                    @foreach($categories as $category)
                        @if($category->id == $image->category_id)
                            <option selected="selected" value="{{$category->name}}">{{$category->name}}</option>
                        @else    
                            <option value="{{$category->name}}">{{$category->name}}</option>
                        @endif        
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="image_description">Descripción de la imagen:</label>
                <textarea id="form_update_image_description" class="form-control" name="image_description" rows="3">{{$image->description}}</textarea>
            </div>
            <div class="form-group">
                <label for="image_file">Imagen:</label>
                <input id="form_update_image_file" type="file" class="form-control-file load_image_file" name="image_file">

            </div>
            <div class="form-group row">
                <div class="col-md-5">
                    <img src="{{$image->url}}" alt="{{$image->title}}" class="img-thumbnail form_image_preview"> 
                </div>    
            </div> 
            
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12 offset-md-4">
                    <a href="{{route('image.index')}}" class="form-control btn btn-outline-dark btn-group-actions">Listado de imágenes</a> 
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12 ">
                    <input type="submit" id="update_image" value="Modificar" class="form-control btn btn-primary btn-group-actions"/> 
                </div>
            </div>

            
                       
        </form>
        </div>
    </div>    


@endsection