<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta id="url_base" content="{{ URL::to('/') }}">
        <meta id="csrf-token" name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

        <!-- CSS Style -->
        <link rel="stylesheet" href="{{asset('css/style.css')}}">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">  
        
        <!-- Material Icons -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

        <!--Jquery-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        
        
    </head>
    <body>

        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('image.index')}}">Inicio <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Gestionar imágenes
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{route('image.create')}}">Crear nueva imagen</a>
                    <a class="dropdown-item" href="{{route('loadImages')}}">Inicializar base de datos</a>
                    <a class="dropdown-item" href="{{route('imageDestroyAll')}}" onclick="return confirm('¿Está seguro que desea eliminar todas las imágenes?')">Eliminar todas las imágenes</a>    
                </li>
            
                </ul>
            </div>
        </nav>
        
        <div class="div-container main-content">
            @yield('content')
        </div>

        <footer class="footer">
            <div class="row">
                <div class="col-12">
                    <p class="text-footer">Prueba técnica Ossian Technology</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>Jimmy Linares Lagarto</p>
                </div>
            </div>
            </footer>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
        <script src="{{asset('js/script.js')}}"></script>
    </body>
</html>