@extends('layouts.app')

@section('title', 'Image CRUD')


@section('content')
        <div id="content_section">   
           @if(isset($response))
            <div class="row">
                <div class="col-md-7 offset-md-2">
                    <div class='col-md-12 form_alert'>
                        <p class='alert alert-secondary' role='alert' style='font-size: 15px;text-align: center'><b>{{$response}}</b></p>
                    </div>
                </div>
            </div>
            @endif

            <div class="card-columns">
                @if(count($imageCollection) == 0)
                <div class="row">
                    <div class="col-md-12 offset-md-10">
                        <h1 class="no_images">No hay imágenes insertadas</h1>
                    </div>
                    </div>    
                @endif
                @foreach($imageCollection as $image)
                
                        <div id="{{$loop->index}}" class="card" ttle="{{$image->title}}">
                            <img src="{{$image->url}}" class="card-img-top img-fluid" alt="{{$image->title}}">
                            <div class="card-body">
                                <p class="card-text"><span class="text-muted">{{$image->category()->first()->name}}</span></p>
                                <h5 class="card-title">{{$image->title}}</h5>
                            </div>
                            <div class="card-footer">
                                <div class="btn-group" role="group" aria-label="Action buttons">
                                    <a href="{{route('image.show', [$image->id])}}" class="btn btn-primary btn-sm"><i class="material-icons">visibility</i></a>
                                    <a href="{{route('image.edit', [$image->id])}}" class="btn btn-light btn-sm"><i class="material-icons">create</i></a>
                                    <a href="{{route('imageDestroy', [$image->id])}}" onclick="return confirm('¿Está seguro que desea eliminar la imagen?')" class="btn btn-danger btn-sm"><i class="material-icons">delete_outline</i></a>
                                </div>
                            </div>    
                        </div>
                        
                       
                @endforeach    
            </div>
        </div>
        
@endsection

  