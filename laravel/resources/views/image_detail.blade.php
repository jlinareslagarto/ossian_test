@extends('layouts.app')

@section('title', 'Detalles de la imagen')

@section('content')

    <div class="card mb-3">
    <div class="row no-gutters">
        <div class="col-md-5">
        <img src={{$image->url}} class="card-img" alt={{$image->title}}>
        </div>
        <div class="col-md-7">
        <div class="card-body">
            <h2 class="card-title">{{$image->title}}</h2>
            <h4 class="card-text">{{$image->category()->first()->name}}</h4>
            <p class="card-text">{{$image->description}}</p>
            
        </div>
        </div>
    </div>
    </div>
    
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12 offset-md-5">
            <a href="{{route('image.index')}}" class="btn btn-outline-dark detail-back">Listado de imágenes</a>
        </div>        
    </div>

@endsection