<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Models\Category;

class ImageTest extends TestCase
{
    
    public function testCreateCategory()
    {
        $response = $this->withHeaders([
            'X-Header' => 'Value',
        ])->json('POST', '/category', ['name' => 'Test category']);

        $response
            ->assertStatus(200);
            
    }

    public function testLoadImages()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
            
    }
}
