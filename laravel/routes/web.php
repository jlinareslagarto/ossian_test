<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImageController;
use App\Models\Image;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home')->with('imageCollection', Image::all());
});

Route::get('/home', function () {
    return view('home')->with('imageCollection', Image::all());
});

Route::resource('image', ImageController::class);
Route::post('/image-update/{id}', [ImageController::class, 'update'])->name('imageUpdate');
Route::post('/category', [ImageController::class, 'createImageCategory'])->name('createCategory');
Route::get('/image-destroy/{id}', [ImageController::class, 'destroy'])->name('imageDestroy');
Route::get('/image-destroy-all', [ImageController::class, 'destroyAll'])->name('imageDestroyAll');
Route::get('/load', [ImageController::class, 'loadFirstImages'])->name('loadImages');


