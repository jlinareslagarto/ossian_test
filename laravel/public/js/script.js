jQuery(document).ready(function($) {

    $(document).on('change','input.load_image_file',function() {
        
        if (this.files && this.files[0]) {
           var reader = new FileReader();
           reader.onload = function(e) {
               
             // Asignamos el atributo src a la tag de imagen
             $('.form_image_preview').attr('src',e.target.result);
           };
           reader.readAsDataURL(this.files[0]);
         }
     });


    $('#form_create_category').on('submit', function(e){
        
        e.preventDefault();
        var urlToSend = $('#url_base').attr('content');
        urlToSend = urlToSend.concat("/category");
        
        $.ajax({
            headers: {'X-CSRF-TOKEN': $("#csrf-token").attr('content')},
            url: urlToSend,
            type: 'POST',
            data: new FormData(this),
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data){
              
              if(data['error'] == "OK")
              {
                if($('div.form_alert').length){

                   $('div.form_alert').remove();   
                }
                  
                $('#div_form_category_msg').prepend("<div class='col-md-12 form_alert'><div>\n\
                          <p class='alert alert-success' role='alert' style='font-size: 15px;text-align: center'><b>Categoría creada</b></p>\n\
                          </div>\n\
                          </div>"
                );
                $('select#form_create_image_category').append("<option value='"+$("#form_create_category_name").val()+"'>"+$("#form_create_category_name").val()+"</option>");
              }
              
              else
              {
                  if($('div.form_alert').length){

                   $('div.form_alert').remove();   
                  }

                  $('#div_form_category_msg').prepend("<div class='col-md-12 form_alert'><div>\n\
                          <p class='alert alert-danger' role='alert' style='font-size: 15px;text-align: center'><b>"+data['error']+"</b></p>\n\
                          </div>\n\
                          </div>"
                      );  
              }
  
            },
            error: function(data, text, msg){
                if($('div.form_alert').length){

                   $('div.form_alert').remove();   
                }
                
                $('#div_form_category_msg').prepend("<div class='col-md-12 form_alert'><div>\n\
                          <p class='alert alert-danger' role='alert' style='font-size: 15px;text-align: center'><b>Error: Inténtelo nuevamente</b></p>\n\
                          </div>\n\
                          </div>"
                );
            }
        });
          
        
    });
});