<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;
use URL;

class Image extends Model
{
    use HasFactory;
    protected $table = 'images';
    public $timestamps = false;
    protected $primaryKey = 'id';

    public static function loadImage($data){

        $image = new Image();
        $image->title = $data->title;
        $category = Category::findByName($data->category);
        
        if(!isset($category)){
            
            $category = Category::create(['name' => $data->category]);
        }

        $image->category_id = $category->id;
        $image->description = $data->description;
        $image->url = $data->url;

        return $image->save();
    }

    public static function create($data){

        $image = new Image();
        $image->title = $data['title'];
        $category = Category::findByName($data['category']);
        $image->category_id = $category->id;
        $image->description = $data['description'];
        $imageUrl = $data['url']->getClientOriginalName();
        Storage::disk('local')->put($imageUrl, \File::get($data['url']));
        $image->url = URL::to('/').'/images/'.$imageUrl;

        return $image->save();
    }

    public static function updateImage($data, $imageFile, $id){

        $image = Image::find($id);
        if(isset($data['image_title']))
        {
            $image->title = $data['image_title'];
        }
        if(isset($data['image_category']))
        {
            $category = Category::where('name', $data['image_category'])->first();
            $image->category_id = $category['id']; 
        }
        if(isset($data['image_description']))
        {
            $image->description = $data['image_description'];
        }
        if(isset($imageFile))
        {
            $imageUrl = $imageFile->getClientOriginalName();
            Storage::disk('local')->put($imageUrl, \File::get($imageFile));
            $image->url = URL::to('/').'/images/'.$imageUrl;
        }
        $successfull = true;
        
        if(!$image->isClean()){
            $successfull = $image->save();
        }

        return $successfull; 
    }

    public static function validateImage($fileExtension)
    {
       $validate = false; 
       
       if($fileExtension == "image/jpg" || $fileExtension == "image/png" || $fileExtension == "image/jpeg")
       { 
          $validate = true; 
       }
       
       return $validate;
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }
}
