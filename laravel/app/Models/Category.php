<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $table = 'category';
    public $timestamps = false;
    protected $primaryKey = 'id';

    public static function create($data){
        $category = new Category();
        $category->name = $data['name'];

        if($category->save()){
            $category->fresh();
        }

        return $category;
    }

    public static function findByName($name){
        
        $category = Category::where('name', $name)->first();
        
        return $category; 
    }

    public function images()
    {
        return $this->hasMany('App\Models\Image', 'category_id');
    }
}
