<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use App\Models\Image;
use App\Models\Category;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Storage;


class ImageController extends Controller
{
    
    public function index()
    {
        $images = Image::all();
        return view('home')->with('imageCollection', $images);
    }

    
    public function create()
    {
        $categories = Category::all();

        return view('create_image')->with('categories', $categories);
    }

    
    public function store(Request $request)
    {
        $validatedData = Validator::make($request->all(),[
            'title' => 'required|max:255|unique:images,title',
            'category' => 'required|exists:category,name',
            'description' => 'required',
            'url' => 'required',
            
            ],
            [
            'title.max' => 'El título debe contener menos de 255 caracteres.',
            'title.required' => 'Introduzca el título de la imagen.',
            'title.unique' => 'Ya existe una imagen con el título introducido.',
            'category.required' => 'Seleccione una categoría para la imagen.',
            'category.exists' => 'La categoría introducida no existe.',
            'description.required' => 'Introduzca una descripción de la imagen.',
            'url.required' => 'Seleccione la imagen que desea agregar.',
                
            ]
            );

            if($validatedData->fails()){

                return redirect()->route('image.create')->withErrors($validatedData)->withInput();
            } 

            $imageToUpload = NULL;
        
            if($request->hasFile('url')){
                
                $imageToUpload = $request->file('url');
            }
        
            if($imageToUpload != NULL && !Image::validateImage($imageToUpload->getMimeType())){
 
                return redirect()->route('image.create')
                                ->withErrors(['incorrect_format' => "Error, el formato de imagen no está permitido."])
                                ->withInput();
            }
            
            if(Image::create($request->all())){
                
                return redirect()->route('image.create')
                                ->withErrors(['successfull' => "Imagen agregada"])
                                ->withInput();      
            }
            else{
                
                return redirect()->route('image.create')
                                ->withErrors(['fail' => "Error: No ha sido posible agregar la imagen, vuelva a intentarlo"])
                                ->withInput();
            }
     
    }

    public function createImageCategory(Request $request){

        $validatedData = Validator::make($request->all(),[
            'name' => 'required|max:255|unique:category,name',          
            ],
            [
            'name.max' => 'La categoría debe contener menos de 255 caracteres.',
            'name.required' => 'Introduzca el nombre de la categoría.',
            'name.unique' => 'Ya existe una categoría con el nombre introducido.',               
            ]
            );

            $categoryResult = array();

            if($validatedData->fails()){

                $categoryResult['error'] = $validatedData->errors()->first();
                return json_encode($categoryResult);
            } 
            if(Category::create($request->all())){
                
                $categoryResult['error'] = "OK";
                return json_encode($categoryResult);
               
            }
            else{
                
                $categoryResult['error'] = "Error: No ha sido posible agregar la imagen, vuelva a intentarlo";
                return json_encode($categoryResult);
            }
    }

    
    public function show($id)
    {
        $image = Image::find($id);

        return view('image_detail')->with('image', $image);
    }

   
    public function edit($id)
    {
        $image = Image::find($id);
        $categories = Category::all();
        
        return view('update_image')->with('image', $image)
                                   ->with('categories', $categories);
    }

  
    public function update(Request $request, $id)
    {
        $validatedData = Validator::make($request->all(),[
            'image_title' => 'max:255',
            'image_category' => 'exists:category,name',
            ],
            [
            'image_title.max' => 'El título debe contener menos de 255 caracteres.',
            'image_category.exists' => 'La categoría introducida no existe.',
                
            ]
            );
        
        if($validatedData->fails()){

            return redirect()->route('image.edit', [$id])->withErrors($validatedData)->withInput(); 
        }    

        $imageToUpload = NULL;
        

        if($request->hasFile('image_file')){
            
            $imageToUpload = $request->file('image_file');
    
        }
    

        if($imageToUpload != NULL && !Image::validateImage($imageToUpload->getMimeType())){
 
            return redirect()->route('image.edit', [$id])
                              ->withErrors(['incorrect_format' => "Error, el formato de imagen no está permitido."])
                              ->withInput();
        }
        
        if(Image::updateImage($request->all(), $imageToUpload, $id)){
            
            return redirect()->route('image.edit', [$id])
                              ->withErrors(['successfull' => "Imagen modificada"])
                              ->withInput();  
            
        }
        else{
            
            return redirect()->route('image.edit', [$id])
                              ->withErrors(['fail' => "Error: No ha sido posible modificar la imagen, vuelva a intentarlo"])
                              ->withInput();
        }
           
    }


    public function destroy($id)
    {
        $image = Image::find($id);
        if(isset($image))
        {
            $this->deleteImageFile($image);
            $image->delete();
        }
        
        return redirect()->route('image.index');
    }


    public function destroyAll(){
        
        $images = Image::all();
        foreach($images as $image){

            $this->deleteImageFile($image);
            $image->delete();
        }

        return redirect()->route('image.index');
    }


    private function deleteImageFile($image){

        $urlExplode = explode("/", $image->url);
        Storage::disk('local')->delete($urlExplode[count($urlExplode)-1]);
    }
    

    public function loadFirstImages(Request $request)
    {
        $response = "La base de datos ya fue inicializada con anterioridad";

        if(Image::all()->isEmpty()){

            $guzzleClient = new Client();
            $url = "http://internal.ossian.tech/api/Sample";

            $response = $guzzleClient->request('GET', $url, ['verify' => false,]);

            $responseBody = json_decode($response->getBody());

            foreach($responseBody->result as $image)
            {
                Image::loadImage($image);
            }

            $response = "Carga realizada con éxito";
        }
        
        return view('home')->with('imageCollection', Image::all())
                           ->with('response', $response);
    }

    
}
